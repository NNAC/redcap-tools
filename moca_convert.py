#!/bin/python

import sys
import csv

if len(sys.argv)!=3:
    print ""
    print "Usage: moca_convert.py <input.csv> <output.csv>"
    print ""
    quit()

with open(sys.argv[1]) as csvfile:
    mocas = csv.DictReader(csvfile)
    rows = [row for row in mocas]

with open(sys.argv[1]) as csvfile:
    header = csv.reader(csvfile)
    hdr = [row for row in header]

hdr = hdr[0]
    
nrows = len(rows)
subjs = set()

for n in range(nrows):
    subjs.add(rows[n]['record_id'])

subjs = list(subjs)

rater_dict1 = {
    '2': 'MBP',
    '7': 'KGD',
    '10': 'KEB',
    '11': 'KDB',
    '8': 'MNS',
    '12': 'SLW',
    '3': 'RN',
    '4': 'JG',
    '5': 'VA',
    '9': 'TL',
    '1': 'NP',
}

rater_dict2 = {
    '2': 'MBP',
    '6': 'KGD',
    '9': 'KEB',
    '10': 'KDB',
    '11': 'SLW',
    '7': 'MNS',
    '3': 'RN',
    '4': 'JG',
    '8': 'TL',
    '1': 'NP',
}

rater_dict3 = {
    '2': 'MBP',
    '7': 'KGD',
    '10': 'KEB',
    '11': 'KDB',
    '8': 'MNS',
    '12': 'SLW',
    '3': 'RN',
    '4': 'JG',
    '5': 'VA',
    '9': 'TL',
    '1': 'NP',
}

for subj in subjs:
    
    # find which rows have moca1
    moca_ones = [ row for row in rows if row['record_id']==subj if row['date_moca_1']!='' if row['redcap_repeat_instance']=='' ]
    moca_one_RENs = [ ren['redcap_event_name'] for ren in moca_ones ]
    instance = 1
    for ren in moca_one_RENs:
        for row in rows:
            if row['record_id']==subj and row['redcap_event_name']==ren:
                row['redcap_repeat_instance'] = str(instance)
                row['redcap_event_name'] = 'visit_1_arm_1'
                row['redcap_repeat_instrument'] = 'montreal_cognitive_assessment_moca'

                if row['rater_moca'] != '6' and row['rater_moca'] != '':
                    row['moca1_rater_other'] = rater_dict1[row['rater_moca']]

                instance += 1

    # find which rows have moca2
    moca_twos = [ row for row in rows if row['record_id']==subj if row['date_moca_2']!='' if row['redcap_repeat_instance']=='' ]
    moca_two_RENs = [ ren['redcap_event_name'] for ren in moca_twos ]
    instance = 1
    for ren in moca_two_RENs:
        for row in rows:
            if row['record_id']==subj and row['redcap_event_name']==ren:
                row['redcap_repeat_instance'] = str(instance)
                row['redcap_event_name'] = 'visit_1_arm_1'
                row['redcap_repeat_instrument'] = 'montreal_cognitive_assessment_moca_2'

                if row['rater_moca_2'] != '5' and row['rater_moca_2'] != '':
                    row['moca2_rater_other'] = rater_dict2[row['rater_moca_2']]

                instance += 1

    # find which rows have moca3
    moca_threes = [ row for row in rows if row['record_id']==subj if row['date_moca_3']!='' if row['redcap_repeat_instance']=='' ]
    moca_three_RENs = [ ren['redcap_event_name'] for ren in moca_threes ]
    instance = 1
    for ren in moca_three_RENs:
        for row in rows:
            if row['record_id']==subj and row['redcap_event_name']==ren:
                row['redcap_repeat_instance'] = str(instance)
                row['redcap_event_name'] = 'visit_1_arm_1'
                row['redcap_repeat_instrument'] = 'montreal_cognitive_assessment_moca_3'

                if row['rater_moca_3'] != '6' and row['rater_moca_3'] != '':
                    row['moca3_rater_other'] = rater_dict3[row['rater_moca_3']]

                instance += 1

with open(sys.argv[2], 'w') as csvfile:
    fieldnames = hdr
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    for row in rows:
        writer.writerow(row)


        
